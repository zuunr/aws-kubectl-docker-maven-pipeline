# aws-kubectl-docker-maven-pipeline

This module contains a Dockerfile to create a docker image based on the maven image, with the additional components aws-cli, kubectl and docker-cli installed.

It is intended to be used in Bitbucket/Gitlab pipelines to activate aws, kubectl and docker-cli functionality.

## Supported tags

* [`1.0.0`, (*1.0.0/Dockerfile*)](https://bitbucket.org/zuunr/aws-kubectl-docker-maven-pipeline/src/1.0.0/Dockerfile)

## Usage

To use the image in a Bitbucket pipeline environment and authenticate with your aws environment, use the following yaml configuration as a base:

```yaml
- step:
    name: My kubectl step
    # Check the image version tag
    image: zuunr/aws-kubectl-docker-maven-pipeline:1.0.0
    script:
      # Set $AWS_ACCESS_KEY_ID and $AWS_SECRET_ACCESS_KEY to the contents of your aws service account in the Bitbucket pipelines environment variables
      # Replace <MY_REGION> with your actual region
      # Replace <MY_ECR_REPO> with your actual ecr repository url
      - aws ecr get-login-password --region <MY_REGION> | docker login --username AWS --password-stdin <MY_ECR_REPO>
      - docker push <MY_IMAGE>:<MY_IMAGE_TAG>
      # Replace <MY_KUBE_CLUSTER> with your actual cluster name
      - aws eks --region <MY_REGION> update-kubeconfig --name <MY_KUBE_CLUSTER>
      - kubectl get pods
```
