FROM maven:3.6.3-jdk-11

RUN curl -O https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip \
    && unzip -q awscli-exe-linux-x86_64.zip \
    && ./aws/install \
    && rm -rf awscli-exe-linux-x86_64.zip \
    && curl -O https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && curl -O https://download.docker.com/linux/static/stable/x86_64/docker-19.03.8.tgz \
    && tar xzvf docker-19.03.8.tgz --strip 1 -C /usr/local/bin docker/docker \
    && rm -rf docker-19.03.8.tgz

CMD ["/bin/sh"]
